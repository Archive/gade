/*
 * gade-window.vala
 * This file is part of gade
 *
 * Copyright (C) 2010 - Jonh Wendell <wendell@bani.com.br>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

using Gtk;
using GLib;

namespace Gade {

  public class Window : Gtk.Window
    {
      private MenuBar menubar;
      private Toolbar toolbar;
      private Notebook _notebook;
      private Statusbar statusbar;
      private VBox main_vbox;
      private UIManager manager;
      private uint sb_tip;

      public Notebook notebook { get {return _notebook;} }

      const ActionEntry[] entries = {
        /* Toplevel */
          { "Secretary", null, N_("_Secretary") },
          { "Finances", null, N_("_Finances") },
          { "View", null, N_("_View") },
          { "Help", null, N_("_Help") },

        /* Secretary Menu */
          { "SecretaryChurch", null, N_("_Church"), null, N_("Edit church data"), null },
          { "SecretaryMembers", "system-users", N_("_Members"), null, N_("Edit members"), null },
          { "SecretaryQuit", STOCK_QUIT, null, "<control>Q", N_("Quit the program"), main_quit },

        /* Finances Menu */
          { "FinancesIncomes", "down", N_("_Incomes"), null, N_("Manage incomes"), null },
          { "FinancesExpenses", "up", N_("_Expenses"), null, N_("Manage expenses"), null },

        /* Help menu */
          { "HelpContents", STOCK_HELP, N_("_Contents"), "F1", N_("Open the manual"),  null },
          { "HelpAbout", STOCK_ABOUT, null, null, N_("About this application"), show_about }
      };

      const ToggleActionEntry[] toggle_entries = {
        /* View Menu */
          { "ViewToolbar", "null", N_("_Toolbar"), null, N_("Show or hide the toolbar"), null, false },
          { "ViewStatusbar", "null", N_("_Statusbar"), null, N_("Show or hide the statusbar"), null, false }
      };

      public Window ()
        {
          title = Environment.get_application_name ();
          set_default_size (300, 50);
          position = WindowPosition.CENTER;
          destroy.connect (Gtk.main_quit);

          main_vbox = new Gtk.VBox (false, 5);
          add (main_vbox);

          create_menubar_and_toolbar ();

          _notebook = new Notebook ();
          main_vbox.pack_start (_notebook, true, true, 0);

          statusbar = new Gtk.Statusbar ();
          sb_tip = statusbar.get_context_id ("hints");
          main_vbox.pack_start (statusbar, false, false, 0);
        }

      private void create_menubar_and_toolbar ()
        {
          manager = new UIManager ();
          try {
            manager.add_ui_from_file ("gade-ui.xml");
          } catch (Error e) {
            try {
              manager.add_ui_from_file (Config.DATADIR + "/gade-ui.xml");
            } catch (Error e) {
              error ("Could not merge UI XML file: %s", e.message);
            }
          }

          add_accel_group (manager.get_accel_group ());
          manager.connect_proxy.connect (connect_proxy);
          manager.disconnect_proxy.connect (disconnect_proxy);

          var ag = new ActionGroup ("AllGroups");
          ag.add_actions (entries, this);
          ag.add_toggle_actions (toggle_entries, this);
          manager.insert_action_group (ag, 0);

          menubar = manager.get_widget ("/MenuBar") as MenuBar;
          main_vbox.pack_start (menubar, false, false, 0);

          toolbar = manager.get_widget ("/ToolBar") as Toolbar;
          main_vbox.pack_start (toolbar, false, false, 0);
      }

      public void show_about (Action action)
        {
          string[] authors = {"Jonh Wendell <wendell@bani.com.br>", null};
          string comment = _("Gade is a Church Management application designed for the GNOME desktop");

          show_about_dialog (this,
                             "authors", authors,
                             "copyright", "Copyright © 2010 Jonh Wendell",
                             "translator-credits", _("translator-credits"),
                             "comments", comment,
                             "version", Config.VERSION,
                             "website", "http://live.gnome.org/Gade",
                             null);
        }

      private void connect_proxy (Action action, Widget widget)
        {
          if (widget is MenuItem)
            {
              (widget as MenuItem).select.connect (menu_item_selected);
              (widget as MenuItem).deselect.connect (menu_item_deselected);
            }
        }

      private void disconnect_proxy (Action action, Widget widget)
        {
          if (widget is MenuItem)
            {
              (widget as MenuItem).select.disconnect (menu_item_selected);
              (widget as MenuItem).deselect.disconnect (menu_item_deselected);
            }
        }

      private void menu_item_selected (Item source)
        {
          Action action = source.get_data ("gtk-action") as Action;
          if (action.tooltip != null)
            statusbar.push (sb_tip, action.tooltip);
        }

      private void menu_item_deselected ()
        {
          statusbar.pop (sb_tip);
        }
  }

}