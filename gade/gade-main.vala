/*
 * gade-main.vala
 * This file is part of gade
 *
 * Copyright (C) 2010 - Jonh Wendell <wendell@bani.com.br>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

using Gtk;
using GLib;
using Unique;

namespace Gade {

  public class Main {
    public static Window window;

    public static int main (string[] args)
      {
        Intl.setlocale (LocaleCategory.ALL, "");
        Intl.bindtextdomain (Config.GETTEXT_PACKAGE, Config.LOCALEDIR);
        Intl.bind_textdomain_codeset (Config.GETTEXT_PACKAGE, "UTF-8");
        Intl.textdomain (Config.GETTEXT_PACKAGE);

        Environment.set_prgname("gade");
        Environment.set_application_name(_("Church Management"));

        Gtk.init (ref args);

        var app = new App ("org.gnome.gade", null);
        if (app.is_running)
          {
            if (app.send_message (Command.ACTIVATE, null) != Response.OK)
              error (_("Failed to communicate to an existing instance of the program"));
            else
              return 0;
          }

        window = new Window ();
        app.watch_window (window);
        app.message_received.connect (message_received);

        window.show_all ();
        Gtk.main ();
        return 0;
      }
    
    private static Response message_received (int command, MessageData message, uint time_)
      {
        if (command == Command.ACTIVATE)
          {
            window.set_screen (message.get_screen ());
            window.present_with_time (time_);
            return Response.OK;
          }
        return Response.FAIL;
      }
  }
  
}