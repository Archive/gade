/*
 * gade-tab-label.vala
 * This file is part of gade
 *
 * Copyright (C) 2010 - Jonh Wendell <wendell@bani.com.br>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

using Gtk;
using GLib;

namespace Gade {

  public class TabLabel : Gtk.HBox
    {
      private string _icon_name;
      private string _title;
      private bool _can_close;

      private Image image;
      private Label label;
      private Button close;

      public signal void closed ();
      public string icon_name
        {
          set
            {
              _icon_name = value;
              if (_icon_name != null)
                {
                  image.set_from_icon_name (this.icon_name, IconSize.MENU);
                  image.show ();
                }
              else
                image.hide ();
            }
          get
            {
              return _icon_name;
            }
        }
      public string title
        {
          set
            {
              _title = value;
              label.set_label (_title);
            }
          get
            {
              return _title;
            }
        }
      public bool can_close
        {
          set
            {
              _can_close = value;
              close.set_visible (_can_close);
            }
          get
            {
              return _can_close;
            }
        }

      public TabLabel (string? icon_name, string title, bool can_close)
        {
          image = new Image ();
          pack_start (image, false, false, 0);

          label = new Label (null);
          label.set_alignment ((float)0, (float)0.5);
          label.show ();
          pack_start (label, true, true, 5);

          setup_button ();

          this.icon_name = icon_name;
          this.title = title;
          this.can_close = can_close;
        }

      private void setup_button ()
        {
          var image = new Image.from_stock (STOCK_CLOSE, IconSize.MENU);
          image.show ();

          close = new Button ();
          close.tooltip_text = _("Close this tab");
          close.relief = ReliefStyle.NONE;
          close.focus_on_click = false;
          close.add (image);
          close.clicked.connect ((s) => {closed ();});

          /* make it as small as possible */
          var rc = new RcStyle ();
          rc.xthickness = rc.ythickness = 0;
          close.modify_style (rc);
          int w, h;
          icon_size_lookup (IconSize.MENU, out w, out h);
          close.set_size_request (w+2, h+2);

          pack_start (close, false, false, 0);
        }

    }
}